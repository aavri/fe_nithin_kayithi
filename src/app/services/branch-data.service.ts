import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchDataService {

  constructor(private http: HttpClient) { }

  getBranches(): Observable<any>{
    return this.http.get('https://api.halifax.co.uk/open-banking/v2.2/branches');
  }

}
