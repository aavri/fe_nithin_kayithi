import { TestBed } from '@angular/core/testing';

import { BranchDataService } from './branch-data.service';

describe('BranchDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BranchDataService = TestBed.get(BranchDataService);
    expect(service).toBeTruthy();
  });
});
