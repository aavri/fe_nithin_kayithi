import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BranchDetailsComponent } from './branch-details/branch-details.component';
import { BranchListComponent } from './branch-list/branch-list.component';

@NgModule({
  declarations: [
    AppComponent,
    BranchDetailsComponent,
    BranchListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
