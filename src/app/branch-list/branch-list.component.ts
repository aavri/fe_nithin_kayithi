import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BranchDataService } from '../services/branch-data.service';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.css']
})
export class BranchListComponent implements OnInit {

  branchObj: object;
  data: [];
  brand: object;
  branches: any[];
  selectedBranch: object;
  city: string;
  searchedBranches: any[] = [];
  displayBranch: boolean = false;
  p: number = 1;

  constructor(private branchDataService: BranchDataService) { }

  ngOnInit() {
    this.branchDataService.getBranches()
      .subscribe(
        response => {
          this.branchObj = response as object;

          this.data = this.branchObj["data"][0];

          this.brand = this.data["Brand"][0];

          this.branches = this.brand["Branch"];

          this.searchedBranches = this.brand["Branch"];
        }
      );
  }

  @Output() branchSelect = new EventEmitter<object>();
  @Output() hideDetails = new EventEmitter<boolean>();

  onSelect(branch: any) {
    this.selectedBranch = branch;
    this.branchSelect.emit(this.selectedBranch);
  }

  citySelected() {
    this.city = this.city.toUpperCase();
    this.searchedBranches = [];
    for (let i in this.branches) {
      if (this.city === this.branches[i].PostalAddress.TownName) {
        this.searchedBranches.push(this.branches[i]);
      }
    }
    this.hideDetails.emit(this.displayBranch);
  }

}
