import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aavri';

  branchSelected: any;
  displayBranchDetails: boolean;

  selectedBranch($event){
    this.branchSelected = $event;
    this.displayBranchDetails = true;
  }

  hideBranchDetails($event){
    this.displayBranchDetails = $event;
  }

}
